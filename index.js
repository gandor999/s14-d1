// Single line comment   - ctrl + /
/* 
	Mutli-line comments  - ctrl + shift + /
 */  


 // Syntax and Statements

 /*
	- Statements in programming are instructions that we tell the computer to perform

	- JS statments usually end with semicolon (;)



	- Syntax in programming, it is the set of rules that describes how statements must be constructed.
	
 */


console.log("Hello Batch 138!");


// Variables
/*
	- It is used to contain data

	- Declaring variables - it tells our devices that a variable name is created and is ready to store data

	- Syntax:

		let variableName;
		const variableName;
		var variableName;


		let keyword - used to handle values that will change over time

		const keyword - used to handle values that will not change over time

*/




let myVariable;

console.log(myVariable);

// console.log(hello);

// Initializing values to variables

/*
	- Syntax:

		let variableName = value;
		const variableName = value;

*/


let productName = "Desktop computer";


console.log(productName);



let productPrice = 10, num1 = 3;
let test = 'Hello ' + 4;

console.log(test + 4);

for(let i = 0; i < 10; ++i)
{
	console.log(i + 1);
}


/*
	Reassignning the initial or previous value into another value

*/

let productName2 = "Desktop computer";

productName2 = "laptop";

console.log(productName2);


const interest = 3.539;

console.log(interest);





// Declaring Multiple Variables


let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode, productBrand);





// Data Types

/*
	Strings

		- a series of characters that create a word, a phrase, a sentence or anything related to creating text

		- strings in JS can be written either a single (') o double (") quotes.

*/


console.log("Welcome to Metro Manila, Philippines");


// Escape Characters

/*
	\n - refers to creating a new line in between text
*/

let mailAddress = "Metro Manila\n\nPhilippines";

console.log(mailAddress);


let message = "Jhon's employees went home early";

console.log(message);

message = 'Jhon\'s employees went home early';
console.log(message);


// Numeric Data Types


// Integers/Whole Numbers



let headCount = 26;
console.log(headCount);


// Decimal Numbers
let grade = 98.7;
console.log(grade);


// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);


// Combining Text and Strings
// Output: Jhon's grade last quarter is 98.7

console.log("Jhon's grade last quarter is " + grade);


// Boolean
// 1 / 0 or True/False
let isMarried = true;
let isGoodConduct = false;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);



// Array
// Array is a special kind of data type that is used to store multiple values

// It can store different data types but is normally used to store similar data types

/*
	Syntax:

	let/const arrayName = [element0, element1, element2, ...];
*/
let grades = [98.7, 92.1, 90.2, 94.6];

console.log(grades);



/*
	Objects

		- another special kind of data type that is used to mimic real world objects/items

	Syntax:

		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
	

*/
let person = {
	fullName: "Geodor Ruales",
	age: 35,
	isMarried: false,
	contact: ["09295678910", "09062877598"],
	address: {
		houseNumber: 458,
		city: "Marikina"
	}
}
console.log("\n\nObjects:");
console.log(person);
console.log(person.fullName);
console.log(person.age);
console.log(person.isMarried);
console.log(person.contact[0]);
console.log(person.address.houseNumber);
console.log(person.address.city);





// Null vs Undefined
/*
	Null

		- used to intentionally express the absence of a value in a variable declaration
*/
let spouse = null;


/*
	Undefined

		-represents the state of a variable that has been declared but w/o an assigned value
*/
let fullName;



console.log("\n\nFunctions:")

/*
	Functions

		- functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked

	Syntax:

		function functionName(){
			block statements;
		}	
*/

/*function printName() {
	console.log("My name is Jhon");

	return 190;
}

console.log(printName());
printName();*/


function favAnimal() {
	console.log("My favourite animal is a dog");
}

favAnimal();

// (name) - parameter
// Parameter - acts as a named variable/ container that exists only inside of a function

function printName(name){
	console.log("My name is " + name);
}

let name = "Geodor Ruales";


// Argument - the actual value that is provided a function for it ot wor properly
printName(name);
printName("Jane");


function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

console.log(argumentFunction);
console.log(argumentFunction());

function createFullName(firstName, middleName, lastName){
	console.log("Hello " + firstName + " " + middleName + " " + lastName);
}

createFullName("Geodor", "Araco", "Ruales");
createFullName("Geodor", "Araco");
createFullName("Geodor", "Araco", "Ruales", "Junior");

// Using variables as arguments
let firstName = "Geodor";
let middleName = "Araco";
let lastName = "Ruales";

console.log("\n\nUsing variables as arguments\n--");

createFullName(firstName, middleName, lastName);

// The return statement
// 		- allows the output of a function to be passed to the lin/block of code that invoked/called the function

console.log("\n\nUsing the 'return' statement\n--");

function returnFullName(firstName, middleName, lastName){
	return "Hello " + firstName + " " + middleName + " " + lastName;

	console.log("A simple message");
}

console.log(returnFullName(firstName, middleName, lastName));